package org.unitedprime.mps.android.activities;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import org.unitedprime.mps.android.R;


public class UserSelectionActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_selection);

        Button useForMeButton = (Button) findViewById(R.id.use_for_me);
        Button useForOthersButton = (Button) findViewById(R.id.use_for_others);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setElevation(0);

        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.user_selection_action_layout,
                null);
        TextView voteInHandsTextView = (TextView) actionBarLayout.findViewById(R.id.general_action_text);

        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(actionBarLayout);

        String fontName = "fonts/Zawgyi.ttf";
        String language = this
                .getSharedPreferences("org.unitedprime.mps.android", Context.MODE_PRIVATE)
                .getString("org.unitedprime.mps.android.language", "un");
        if (language.equals("un")) {
            fontName = "fonts/Unicode.ttf";
            voteInHandsTextView.setText(R.string.a_vote_in_your_hands);
            useForMeButton.setText(R.string.for_me);
            useForOthersButton.setText(R.string.for_someone_else);
        } else {
            voteInHandsTextView.setText(R.string.a_vote_in_your_hands_zg);
            useForMeButton.setText(R.string.for_me_zg);
            useForOthersButton.setText(R.string.for_someone_else_zg);
        }
        Typeface face = Typeface.createFromAsset(this.getAssets(), fontName);
        voteInHandsTextView.setTypeface(face);
        useForMeButton.setTypeface(face);
        useForOthersButton.setTypeface(face);
    }

}
