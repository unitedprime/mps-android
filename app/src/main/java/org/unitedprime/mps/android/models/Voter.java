package org.unitedprime.mps.android.models;


import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandah Aung on 26/9/15.
 */

public class Voter {
    @SerializedName("voter_name")
    private String voterName;
    @SerializedName("nrcno")
    private String nrcNumber;
    @SerializedName("dateofbirth_number")
    private String dateOfBirthEnglish;
    @SerializedName("dateofbirth")
    private String dateOfBirthMyanmar;
    @SerializedName("mother_name")
    private String motherName;
    @SerializedName("father_name")
    private String fatherName;
    private String village;
    private String township;
    private String district;
    private String state;
    private Ethnics ethnics;
}
