package org.unitedprime.mps.android.activities;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import org.unitedprime.mps.android.R;
import org.unitedprime.mps.android.enums.Encoding;
import org.unitedprime.mps.android.enums.PersonIdentifier;

import java.util.Calendar;

public class CheckVoterActivity extends ActionBarActivity {

    private Encoding encoding;
    private PersonIdentifier personIdentifier;
    private Typeface face;

    private RadioGroup fontChooseRadioGroup;
    private RadioButton useUnicodeRadioButton;
    private RadioButton useZawgyiRadioButton;
    private EditText nameEditText;
    private EditText nrcDobEditText;
    private RelativeLayout dobRelativeLayout;
    private EditText dateTfEditText;
    private EditText monthTfEditText;
    private EditText yearTfEditText;
    private ImageButton calendarButton;
    private RadioGroup chooseNrcDobRadioGroup;
    private RadioButton useNrcRadioButton;
    private RadioButton useDobRadioButton;

    private ActionBar actionBar;
    private ViewGroup actionBarLayout;

    private TextView voteInHandsTextView;

    private Button proceedButton;
    private Button cancelButton;

    private static boolean nonDashFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_voter);

        actionBar = getSupportActionBar();
        actionBar.setElevation(0);

        actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.general_action_layout,
                null);
        voteInHandsTextView = (TextView) actionBarLayout.findViewById(R.id.general_action_text);

        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(actionBarLayout);

        fontChooseRadioGroup = (RadioGroup) findViewById(R.id.font_choose);
        useUnicodeRadioButton = (RadioButton) findViewById(R.id.use_unicode_for_typing);
        useZawgyiRadioButton = (RadioButton) findViewById(R.id.use_zawgyi_for_typing);
        nameEditText = (EditText) findViewById(R.id.name);
        nrcDobEditText = (EditText) findViewById(R.id.nrc_dob);
        chooseNrcDobRadioGroup = (RadioGroup) findViewById(R.id.nrc_dob_choose);
        dobRelativeLayout = (RelativeLayout) findViewById(R.id.dob_layout);
        dateTfEditText = (EditText) findViewById(R.id.date_text_field);
        monthTfEditText = (EditText) findViewById(R.id.month_text_field);
        yearTfEditText = (EditText) findViewById(R.id.year_text_field);
        calendarButton = (ImageButton) findViewById(R.id.calendar);
        useNrcRadioButton = (RadioButton) findViewById(R.id.use_nrc);
        useDobRadioButton = (RadioButton) findViewById(R.id.use_dob);

        proceedButton = (Button) findViewById(R.id.proceed);
        cancelButton = (Button) findViewById(R.id.cancel);

        final String language = this
                .getSharedPreferences("org.unitedprime.mps.android", Context.MODE_PRIVATE)
                .getString("org.unitedprime.mps.android.language", "un");
        encoding = language.equals("un") ? Encoding.UNICODE : Encoding.ZAWGYI;
        personIdentifier = PersonIdentifier.NRC;

        updateLanguage();

        if (encoding == Encoding.UNICODE)
            fontChooseRadioGroup.check(R.id.use_unicode_for_typing);
        else
            fontChooseRadioGroup.check(R.id.use_zawgyi_for_typing);

        fontChooseRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.use_unicode_for_typing)
                    encoding = Encoding.UNICODE;
                else
                    encoding = Encoding.ZAWGYI;
                updateLanguage();

            }
        });

        chooseNrcDobRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.use_nrc) {
                    nrcDobEditText.setVisibility(View.VISIBLE);
                    dobRelativeLayout.setVisibility(View.INVISIBLE);
                    nameEditText.setNextFocusForwardId(R.id.nrc_dob);
                    nrcDobEditText.setNextFocusForwardId(R.id.proceed);
                    personIdentifier = PersonIdentifier.NRC;
                } else {
                    nrcDobEditText.setVisibility(View.INVISIBLE);
                    dobRelativeLayout.setVisibility(View.VISIBLE);
                    nameEditText.setNextFocusForwardId(R.id.date_text_field);
                    yearTfEditText.setNextFocusForwardId(R.id.proceed);
                    personIdentifier = PersonIdentifier.DOB;
                }
            }
        });

        nameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                nameEditText.setError(null);
                validateName();
            }
        });

        nrcDobEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    validateNrc();
                }
            }
        });

        nrcDobEditText.addTextChangedListener(new TextWatcher() {

            private String previousS = "";

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                nrcDobEditText.setError(null);
                previousS = s.toString();
                String nameWithoutSpace = s.toString().replaceAll("\\s+", "");
                if (personIdentifier == PersonIdentifier.NRC) {
                    if (previousS.length() < s.length()) {
                        if (encoding == Encoding.UNICODE) {
                            if (nameWithoutSpace.matches("\u1041[\u1040-\u1049]")) {
                                nrcDobEditText.setText(nameWithoutSpace + "/");
                                nrcDobEditText.setSelection(nrcDobEditText.getText().length());
                            }
                            if (nameWithoutSpace.matches("([\u1041-\u1049]|\u1041[\u1040-\u1045])/[\u1000-\u1021]{3}\\(")) {
                                nrcDobEditText.setText(nameWithoutSpace + "နိုင\u103A)");
                                nrcDobEditText.setSelection(nrcDobEditText.getText().length());
                            }

                        } else {

                            if (nameWithoutSpace.matches("\u1041[\u1040-\u1049]")) {
                                nrcDobEditText.setText(nameWithoutSpace + "/");
                                nrcDobEditText.setSelection(nrcDobEditText.getText().length());
                            }

                            if (nameWithoutSpace.matches("([\u1041-\u1049]|\u1041[\u1040-\u1045])/[\u1000-\u1021]{3}\\(")) {
                                nrcDobEditText.setText(nameWithoutSpace + "\u108Fုိင္)");
                                nrcDobEditText.setSelection(nrcDobEditText.getText().length());
                            }
                        }
                    }
                }
            }

        });

        dateTfEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                dateTfEditText.setError(null);
                String error;
                if (encoding == Encoding.UNICODE) {
                    error = getResources().getString(R.string.wrong_date_format);
                } else {
                    error = getResources().getString(R.string.wrong_date_format_zg);
                }

                String dobWithoutSpace = s.toString().replaceAll("\\s+", "");

                if (!dobWithoutSpace.isEmpty()) {
                    if (!dobWithoutSpace.toString().matches("^\\d{1}$") && !dobWithoutSpace.toString().matches("^\\d{2}$"))
                        dateTfEditText.setError(error);
                    else if (Integer.parseInt(dobWithoutSpace.toString()) > 31) {
                        dateTfEditText.setError(error);
                    }
                }
            }
        });

        monthTfEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                monthTfEditText.setError(null);
                String error;
                if (encoding == Encoding.UNICODE) {
                    error = getResources().getString(R.string.wrong_date_format);
                } else {
                    error = getResources().getString(R.string.wrong_date_format_zg);
                }

                String dobWithoutSpace = editable.toString().replaceAll("\\s+", "");

                if (!dobWithoutSpace.isEmpty()) {
                    if (!dobWithoutSpace.toString().matches("^\\d{1}$") && !dobWithoutSpace.toString().matches("^\\d{2}$"))
                        monthTfEditText.setError(error);
                    else if (Integer.parseInt(dobWithoutSpace.toString()) > 12) {
                        monthTfEditText.setError(error);
                    }
                }
            }
        });

        yearTfEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                yearTfEditText.setError(null);
                String error;
                if (encoding == Encoding.UNICODE) {
                    error = getResources().getString(R.string.wrong_date_format);
                } else {
                    error = getResources().getString(R.string.wrong_date_format_zg);
                }

                String dobWithoutSpace = editable.toString().replaceAll("\\s+", "");

                if (!(dobWithoutSpace.length() < 4)) {
                    if (!dobWithoutSpace.toString().matches("^\\d{4}$"))
                        yearTfEditText.setError(error);
                    else if (Integer.parseInt(dobWithoutSpace.toString()) < 1900) {
                        yearTfEditText.setError(error);
                    }
                }
            }
        });

        calendarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                int date = now.get(Calendar.DATE);
                int month = now.get(Calendar.MONTH);
                int year = now.get(Calendar.YEAR);

                try {
                    now.set(Calendar.YEAR, Integer.parseInt(yearTfEditText.getText().toString()));
                    now.set(Calendar.MONTH, Integer.parseInt(monthTfEditText.getText().toString()) - 1);
                    now.set(Calendar.DATE, Integer.parseInt(dateTfEditText.getText().toString()));
                } catch (Exception e) {
                    now.set(Calendar.YEAR, year);
                    now.set(Calendar.MONTH, month);
                    now.set(Calendar.DATE, date);
                }

                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePickerDialog datePickerDialog, int i, int i1, int i2) {
                                dateTfEditText.setText(String.valueOf(i2));
                                monthTfEditText.setText(String.valueOf(i1 + 1));
                                yearTfEditText.setText(String.valueOf(i));
                            }
                        },
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(), "Date of Birth");
            }
        });

        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean nameOk = validateName();

                if (nameEditText.getText().length() == 0) {
                    nameOk = false;
                    if (encoding == Encoding.UNICODE) {
                        nameEditText.setError(getResources().getString(R.string.fill_name));
                    } else {
                        nameEditText.setError(getResources().getString(R.string.fill_name_zg));
                    }
                }

                if (personIdentifier == PersonIdentifier.NRC) {
                    boolean nrcOk;
                    if (nrcDobEditText.getText().length() == 0) {
                        nrcOk = false;
                        if (encoding == Encoding.UNICODE) {
                            nrcDobEditText.setError(getResources().getString(R.string.fill_nrc));
                        } else {
                            nrcDobEditText.setError(getResources().getString(R.string.fill_nrc_zg));
                        }
                    }
                    nrcOk = validateNrc();

                    if (nameOk && nrcOk) {
                        resetLanguage();
                        
                    }

                } else {
                    boolean dateOk = true;
                    String dateWithoutSpace = dateTfEditText.getText().toString().replaceAll("\\s+", "");

                    if (dateWithoutSpace.isEmpty()) {
                        dateOk = false;
                        if (encoding == Encoding.UNICODE) {
                            dateTfEditText.setError(getResources().getString(R.string.fill));
                        } else {
                            dateTfEditText.setError(getResources().getString(R.string.fill_zg));
                        }
                    }
                    if (!dateWithoutSpace.toString().matches("^\\d{1}$") && !dateWithoutSpace.toString().matches("^\\d{2}$")) {
                        dateOk = false;
                        if (encoding == Encoding.UNICODE) {
                            dateTfEditText.setError(getResources().getString(R.string.date_wrong));
                        } else {
                            dateTfEditText.setError(getResources().getString(R.string.date_wrong_zg));
                        }
                    } else if (Integer.parseInt(dateWithoutSpace.toString()) > 31) {
                        dateOk = false;
                        if (encoding == Encoding.UNICODE) {
                            dateTfEditText.setError(getResources().getString(R.string.date_wrong));
                        } else {
                            dateTfEditText.setError(getResources().getString(R.string.date_wrong_zg));
                        }
                    }

                    boolean monthOk = true;
                    String monthWithoutSpace = monthTfEditText.getText().toString().replaceAll("\\s+", "");
                    monthOk = !monthWithoutSpace.isEmpty();
                    if (monthWithoutSpace.isEmpty()) {
                        monthOk = false;
                        if (encoding == Encoding.UNICODE) {
                            monthTfEditText.setError(getResources().getString(R.string.fill));
                        } else {
                            monthTfEditText.setError(getResources().getString(R.string.fill_zg));
                        }
                    }
                    if (!monthWithoutSpace.toString().matches("^\\d{1}$") && !monthWithoutSpace.toString().matches("^\\d{2}$")) {
                        monthOk = false;
                        if (encoding == Encoding.UNICODE) {
                            monthTfEditText.setError(getResources().getString(R.string.month_wrong));
                        } else {
                            monthTfEditText.setError(getResources().getString(R.string.month_wrong_zg));
                        }
                    } else if (Integer.parseInt(monthWithoutSpace.toString()) > 12) {
                        monthOk = false;
                        if (encoding == Encoding.UNICODE) {
                            monthTfEditText.setError(getResources().getString(R.string.month_wrong));
                        } else {
                            monthTfEditText.setError(getResources().getString(R.string.month_wrong_zg));
                        }
                    }

                    boolean yearOk = true;
                    String yearWithoutSpace = yearTfEditText.getText().toString().replaceAll("\\s+", "");
                    dateOk = !yearWithoutSpace.isEmpty();
                    if (yearWithoutSpace.isEmpty()) {
                        dateOk = false;
                        if (encoding == Encoding.UNICODE) {
                            yearTfEditText.setError(getResources().getString(R.string.fill));
                        } else {
                            yearTfEditText.setError(getResources().getString(R.string.fill_zg));
                        }
                    }
                    if (!yearWithoutSpace.toString().matches("^\\d{4}$")) {
                        yearOk = false;
                        if (encoding == Encoding.UNICODE) {
                            yearTfEditText.setError(getResources().getString(R.string.year_wrong));
                        } else {
                            yearTfEditText.setError(getResources().getString(R.string.year_wrong_zg));
                        }
                    } else if (Integer.parseInt(yearWithoutSpace.toString()) < 1900) {
                        yearOk = false;
                        if (encoding == Encoding.UNICODE) {
                            yearTfEditText.setError(getResources().getString(R.string.year_wrong));
                        } else {
                            yearTfEditText.setError(getResources().getString(R.string.year_wrong_zg));
                        }
                    }

                    if (!dateWithoutSpace.isEmpty() && Integer.parseInt(dateWithoutSpace) > numberOfDaysInMonthAndYear(Integer.parseInt(monthWithoutSpace), Integer.parseInt(yearWithoutSpace))) {
                        dateOk = false;
                        if (encoding == Encoding.UNICODE) {
                            yearTfEditText.setError(getResources().getString(R.string.year_wrong));
                        } else {
                            yearTfEditText.setError(getResources().getString(R.string.year_wrong_zg));
                        }
                    }


                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        resetLanguage();
        super.onBackPressed();
    }

    private void resetLanguage() {
        String language = this
                .getSharedPreferences("org.unitedprime.mps.android", Context.MODE_PRIVATE)
                .getString("org.unitedprime.mps.android.language", "un");
        encoding = language.equals("un") ? Encoding.UNICODE : Encoding.ZAWGYI;
        personIdentifier = PersonIdentifier.NRC;
    }

    private void updateLanguage() {

        String fontName = "fonts/Zawgyi.ttf";
        if (encoding == Encoding.UNICODE) {
            fontName = "fonts/Unicode.ttf";
            voteInHandsTextView.setText(R.string.are_you_in_voter_list);
            nameEditText.setHint(R.string.name);
            nrcDobEditText.setHint(R.string.nrc);
            proceedButton.setText(R.string.proceed);
            cancelButton.setText(R.string.cancel);
            useUnicodeRadioButton.setText(R.string.use_unicode);
            useZawgyiRadioButton.setText(R.string.use_zawgyi);
            useNrcRadioButton.setText(R.string.search_by_nrc);
            useDobRadioButton.setText(R.string.search_by_dob);
            dateTfEditText.setHint(R.string.date_of_birth);
            monthTfEditText.setHint(R.string.month_of_birth);
            yearTfEditText.setHint(R.string.year_of_birth);
        } else {
            voteInHandsTextView.setText(R.string.are_you_in_voter_list_zg);
            nameEditText.setHint(R.string.name_zg);
            nrcDobEditText.setHint(R.string.nrc_zg);
            proceedButton.setText(R.string.proceed_zg);
            cancelButton.setText(R.string.cancel_zg);
            useUnicodeRadioButton.setText(R.string.use_unicode_zg);
            useZawgyiRadioButton.setText(R.string.use_zawgyi_zg);
            useNrcRadioButton.setText(R.string.search_by_nrc_zg);
            useDobRadioButton.setText(R.string.search_by_dob_zg);
            dateTfEditText.setHint(R.string.date_of_birth_zg);
            monthTfEditText.setHint(R.string.month_of_birth_zg);
            yearTfEditText.setHint(R.string.year_of_birth_zg);
        }

        face = Typeface.createFromAsset(this.getAssets(), fontName);
        voteInHandsTextView.setTypeface(face);
        nameEditText.setTypeface(face);
        nrcDobEditText.setTypeface(face);
        proceedButton.setTypeface(face);
        cancelButton.setTypeface(face);
        useUnicodeRadioButton.setTypeface(face);
        useZawgyiRadioButton.setTypeface(face);
        useNrcRadioButton.setTypeface(face);
        useDobRadioButton.setTypeface(face);
        dateTfEditText.setTypeface(face);
        monthTfEditText.setTypeface(face);
        yearTfEditText.setTypeface(face);
    }

    private boolean validateName() {
        boolean nameOk = checkName(nameEditText.getText());
        if (!nameOk) {
            String error;
            if (encoding == Encoding.UNICODE) {
                error = getResources().getString(R.string.use_myanmar_name);
            } else {
                error = getResources().getString(R.string.use_myanmar_name_zg);
            }
            nameEditText.setError(error);
        }
        return nameOk;
    }

    private boolean checkName(CharSequence name) {
        if (encoding == Encoding.UNICODE) {
            return !name.toString().matches(".*[^\u1000-\u103f\\s].*");
        } else {
            return !name.toString().matches(".*[^\u1000-\u109f\\s].*");
        }
    }

    private boolean validateNrc() {
        boolean nrcOk = checkNrc(nrcDobEditText.getText());
        if (!nrcOk) {
            String error;
            if (encoding == Encoding.UNICODE) {
                error = getResources().getString(R.string.not_the_right_nrc_format);
            } else {
                error = getResources().getString(R.string.not_the_right_nrc_format_zg);
            }
            nrcDobEditText.setError(error);
        }
        return nrcOk;
    }

    private boolean checkNrc(CharSequence nrc) {
        if (nrc.toString().isEmpty())
            return true;
        String nameWithoutSpace = nrc.toString().replaceAll("\\s+", "");
        if (encoding == Encoding.UNICODE) {
            return nameWithoutSpace.toString().matches("([\u1041-\u1049]|\u1041[\u1040-\u1045])/[\u1000-\u1021]{3}\\(\u1014\u102D\u102F\u1004\u103A\\)[\u1040-\u1049]{6}");
        } else {
            return nameWithoutSpace.toString().matches("([\u1041-\u1049]|\u1041[\u1040-\u1045])/[\u1000-\u1021]{3}\\(\u108F\u102D\u102F\u1004\u1039\\)[\u1040-\u1049]{6}");
        }
    }

    private int numberOfDaysInMonthAndYear(int month, int year) {
        int daysInMonth = 31 - ((month == 2) ?
                (3 - isLeapYear(year)) : ((month - 1) % 7 % 2));
        return daysInMonth;
    }

    private int isLeapYear(int year) {
        if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
            return 1;
        } else {
            return 0;
        }
    }

}
