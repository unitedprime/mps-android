package org.unitedprime.mps.android.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandah Aung on 27/9/15.
 */
public class Ethnics {
    @SerializedName("name_my")
    private String myanmarName;
    @SerializedName("name_en")
    private String englishName;
    private String code;
}
