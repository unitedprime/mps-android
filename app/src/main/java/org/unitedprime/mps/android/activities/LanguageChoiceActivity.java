package org.unitedprime.mps.android.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.ViewGroup;
import android.widget.TextView;
import org.unitedprime.mps.android.R;

public class LanguageChoiceActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_choice);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setElevation(0);

        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.language_action_layout,
                null);
        TextView voteInHandsTextView = (TextView) actionBarLayout.findViewById(R.id.general_action_text);

        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(actionBarLayout);
    }
}
