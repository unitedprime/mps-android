package org.unitedprime.mps.android.enums;

/**
 * Created by Sandah Aung on 23/9/15.
 */
public enum Encoding {
    UNICODE, ZAWGYI;
}
