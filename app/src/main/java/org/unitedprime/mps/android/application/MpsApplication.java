package org.unitedprime.mps.android.application;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.util.DisplayMetrics;

import java.util.Locale;

/**
 * Created by Sandah Aung on 26/3/15.
 */

public class MpsApplication extends Application {

    private static Context initialContext;
    private static SQLiteDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();
        initialContext = getApplicationContext();
        setupLanguage();
    }

    public static Context getAppContext() {
        return initialContext;
    }

    public static SQLiteDatabase getDatabase() {
        return database;
    }

    private void setupLanguage() {
        setLocale(readLanguage());
    }

    private void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    private SharedPreferences obtainSharedPreferences() {
        return getSharedPreferences("org.unitedprime.mps.android", Context.MODE_PRIVATE);
    }

    private String readLanguage() {
        return obtainSharedPreferences().getString("org.unitedprime.mps.android.language", "un");
    }
}
