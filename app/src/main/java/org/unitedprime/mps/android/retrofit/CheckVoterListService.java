package org.unitedprime.mps.android.retrofit;

import org.unitedprime.mps.android.models.Voter;
import retrofit.Call;

/**
 * Created by Sandah Aung on 26/9/15.
 */
public interface CheckVoterListService {
    Call<Voter> checkVoterByNrc(String nrc);
}
